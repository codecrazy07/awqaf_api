var express = require("express");
var router = express.Router();
var pool = require("../stoarge/db");
var sms = require("../utils/sms");
var tokenUtils = require("../utils/jwt");

//router.all("*", tokenUtils.verifyJWT_MW);

router.get("/profile/id/:id", async (req, res) => {
  //console.log("token is okay, emp id =", req.employee_id);
  var StudentProfile = await pool.query(` CALL SP_GET_STUDENT_BY_ID(?)`, [
    req.params.id
  ]);
  if (StudentProfile.length > 0) {
    res.send({
      success: true,
      message: "student profile found",
      data: JSON.parse(JSON.stringify(StudentProfile[0]))
    });
  } else {
    res.send({
      success: false,
      message: "student profile not found",
      data: []
    });
  }
});

router.get("/profile", async (req, res) => {
  console.log("token is okay, emp id =", req.employee_id);
  var StudentProfile = await pool.query(` CALL sp_get_studentprofile(?)`, [
    req.employee_id
  ]);
  if (StudentProfile.length > 0) {
    res.send({
      success: true,
      message: "student profile found",
      data: JSON.parse(JSON.stringify(StudentProfile[0]))
    });
  } else {
    res.send({
      success: false,
      message: "student profile not found",
      data: []
    });
  }
});

router.post("/profile/update", async (req, res) => {
  console.log(req.body);
  var UpdateStudentProfile = await pool.query(
    `  UPDATE students SET ${req.body.key} = ${req.body.value} WHERE emp_id = ${
      req.employee_id
    } `
  );
  if (UpdateStudentProfile.changedRows > 0) {
    res.send({
      success: true,
      message: "update is complete"
    });
  } else {
    res.send({
      success: false,
      message: "nothing was update"
    });
  }
});

module.exports = router;
