const express = require("express"),
  studnetApi = require("./api/student"),
  registrationApi = require("./api/registration");
(morgan = require("morgan")), (app = express());

var bodyParser = require("body-parser");
app.use(morgan("dev"));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use("/registration", registrationApi);

//requires token
app.use("/student", studnetApi);
app.get("/welcome", async (req, res) => {
  res.send({
    success: true,
    message: "API is Working"
  });
});

const port = 3000;

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
